<?php

/**
 * select2package add a package select2 for public survey
 *
 * @author Denis Chenu <denis@sondages.pro>
 * @copyright 2017-2023 Denis Chenu <www.sondages.pro>
 * @license AGPL v3
 * @version 2.1.1
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU AFFERO GENERAL PUBLIC LICENSE as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

class select2package extends PluginBase
{
    protected $storage = 'DbStorage';
    protected static $description = 'Replace old select2 by new one for A11Y and allow to register package in public survey';
    protected static $name = 'select2package';

    protected $settings = array(
        'autoRegisterPublic' => array(
            'type' => 'boolean',
            'label' => 'Allways register in surveys',
            'default' => false
        ),
        'useDevelopVersion' => array(
            'type' => 'boolean',
            'label' => 'Use develop version',
            'default' => false
        ),
    );

    public function init()
    {
        Yii::setPathOfAlias('select2package', dirname(__FILE__));
        if(App()->getConfig('versionnumber') <= 4) {
            /* Register the new package */
            $this->registerPackage();
            /* Check if needed to be replaced (bootstrap-select2)
             * @see mantis issue https://bugs.limesurvey.org/view.php?id=17813
             **/
            $this->subscribe('beforeControllerAction');
        }
        /* Register the package for public survey */
        $this->subscribe('beforeSurveyPage');
    }

    /**
    * Function to register the packahe if noit exist
    */
    public function registerPackage()
    {
        $debug = boolval(App()->getConfig("debug"));
        $minVersion = ($debug > 0) ? "" : ".min";
        $jsLanguageRelated = array(
            'af' => 'af',
            'sq' => 'sq',
            'ar' => 'ar',
            'hy' => 'hy',
            'az' => 'az',
            'eu' => 'eu',
            'bn' => 'bn',
            'bs' => 'bs',
            'bg' => 'bg',
            'ca-valencia' => 'ca',
            'ca' => 'ca',
            'zh-Hans' => 'zh-CN',
            'zh-Hant-HK' => 'zh-CN',
            'zh-Hant-TW' => 'zh-TW',
            'cs' => 'cs',
            'cs-informal' => 'cs',
            'da' => 'da',
            'nl' => 'nl',
            'nl-informal' => 'nl',
            'en' => 'en',
            'et' => 'et',
            'fi' => 'fi',
            'fr' => 'fr',
            'gl' => 'gl',
            'ka' => 'ka',
            'de' => 'de',
            'de-easy' => 'de',
            'de-informal' => 'de',
            'el' => 'el',
            'he' => 'he',
            'hi' => 'hi',
            'is' => 'is',
            'id' => 'id',
            'it' => 'it',
            'it-informal' => 'it',
            'ja' => 'ja',
            'ko' => 'ko',
            'lv' => 'lv',
            'lt' => 'lt',
            'mk' => 'mk',
            'ms' => 'ms',
            'nb' => 'nb',
            'ps' => 'ps',
            'fa' => 'fa',
            'pl' => 'pl',
            'pl-informal' => 'pl',
            'pt' => 'pt',
            'pt-BR' => 'pt-BR',
            'pa' => 'pa',
            'ro' => 'ro',
            'ru' => 'ru',
            'sr' => 'sr-Cyrl',
            'sr-Latn' => 'sr',
            'sk' => 'sk',
            'sl' => 'sl',
            'es' => 'es',
            'es-AR' => 'es',
            'es-AR-informal' => 'es',
            'es-CL' => 'es',
            'es-CO' => 'es',
            'es-MX' => 'es',
            'sv' => 'sv',
            'te' => 'te',
            'th' => 'th',
            'tr' => 'tr',
            'uk' => 'uk',
        );
        $jslanguage = 'js/i18n/en.js';
        if (isset($jsLanguageRelated[App()->getlanguage()])) {
            $jslanguage = 'js/i18n/' . $jsLanguageRelated[App()->getlanguage()] . '.js';
        }
        $basePath = 'select2package.vendor.select2';
        if ($this->get('useDevelopVersion')) {
            $basePath = 'select2package.vendor.select2';
        }
        if (!Yii::app()->clientScript->hasPackage('select2')) {
            Yii::app()->clientScript->addPackage('select2', array(
                'basePath'    => 'select2package.vendor.select2',
                'css'         => array('css/select2.css'),
                'js'          => array(
                    'js/select2.full' . $minVersion . '.js',
                    $jslanguage
                ),
                'depends'     => array('jquery')
            ));
        }
        if (!Yii::app()->clientScript->hasPackage('select2-bootstrap-theme')) {
            Yii::app()->clientScript->addPackage('select2-bootstrap-theme', array(
                'basePath'    => 'select2package.vendor.select2-bootstrap-theme',
                'css'         => array('select2-bootstrap.css'),
                'depends'     => array(
                    //'bootstrap',
                    'select2'
                )
            ));
        }
        if (!Yii::app()->clientScript->hasPackage('select2-limesurvey')) {
            Yii::app()->clientScript->addPackage('select2-limesurvey', array(
                'basePath'    => 'select2package.assets.select2-limesurvey',
                'css'         => array('select2-limesurvey.css'),
                'depends'     => array(
                    'select2-bootstrap-theme'
                )
            ));
        }
        App()->clientScript->packages['bootstrap-select2'] = array(
            'depends'     => array(
                'select2',
                'select2-bootstrap-theme',
                'select2-limesurvey'
            )
        );
    }

    /**
     * before controller action
     * Check if bootstrap-select2 is already registered, unregister it and register select2-bootstrap-theme
     * @return void
     */
    public function beforeControllerAction()
    {
        if (!empty(App()->getClientScript()->coreScripts['bootstrap-select2'])) {
            App()->getClientScript()->unregisterPackage('bootstrap-select2');
            App()->getClientScript()->registerPackage('select2-bootstrap-theme');
        }
    }

    /**
    * If needed only in survey
    * @return void
    */
    public function beforeSurveyPage()
    {
        if(App()->getConfig('versionnumber') > 4) {
            $this->registerPackage();
            if (!empty(App()->getClientScript()->coreScripts['bootstrap-select2'])) {
                App()->getClientScript()->unregisterPackage('bootstrap-select2');
                App()->getClientScript()->registerPackage('select2-bootstrap-theme');
            }
        }
        if ($this->get('autoRegisterPublic')) {
            App()->getClientScript()->registerPackage('select2-bootstrap-theme');
            $script = "$('.activate-select2 select').select2({ theme: 'bootstrap' });";
            App()->getClientScript()->registerScript('select2packagescript', $script, CClientScript::POS_READY);
        }
    }
}
